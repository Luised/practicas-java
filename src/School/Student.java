package School;

public class Student {

    private String name;
    private float gpa;
    private int credits;
    private int qualityPoints;

    public Student(String name) {
        this.name = name;
    }

    public float getGpa() {
        return gpa;
    }

    public String getName() {
        return name;
    }

    public void updateCreditsAndQualityPoints(int credits, int qualityPoints){
        this.credits = credits;
        this.qualityPoints = qualityPoints;
        gpa = (float) this.credits / this.qualityPoints;
    }
}
