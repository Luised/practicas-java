package Pocker;

import java.util.Scanner;

public class Jugar {

    public static void main(String[] args) {

        Deck.mixCards();

        Hand h1 = new Hand("Poncilo");
        Hand h2 = new Hand("Pedro");
        Hand h3 = new Hand("Pancho");
        Hand h4 = new Hand("Pepe");
        Hand h5 = new Hand("Pablo");

        Hand[] hands = new Hand[5];

        hands[0] = h1;
        hands[1] = h2;
        hands[2] = h3;
        hands[3] = h4;
        hands[4] = h5;

        Jugar.play(hands);

    }

    private static void play(Hand[] hands){
        Scanner scanner = new Scanner(System.in);
        method:
        {
            for (Hand h:hands) {
                h.addCard(Deck.getCard());
                h.addCard(Deck.getCard());
            }
            print(hands);
            Hand handWinner = findWinner(hands);
            if(handWinner!=null){
                System.out.println("\n\n The Hand of "+ handWinner.getNombre()+" is the hand winner. \n\n");
                break method;
            }

            for (int i = 0; i < 3; i++) {
                System.out.println();
                for (Hand h : hands) {
                    if (h.getPoints() < 21) {
                        System.out.println(h.getNombre() + " deseas otra carta? (y/n) ");
                        String r = scanner.nextLine();
                        Boolean res = r.equals("y");
                        if (res) {
                            h.addCard(Deck.getCard());
                        }
                    } else {
                        System.out.println(h.getNombre() + " te pasaste de 21. \n");
                    }
                }
                print(hands);
                Hand handWinnerInt = findWinner(hands);
                if(handWinnerInt!=null){
                    System.out.println("\n\n The Hand of "+ handWinnerInt.getNombre()+" is the hand winner. \n\n");
                    break method;
                }
            }

            Hand handWinnerExr = findWinnerDefault(hands);
            if(handWinnerExr!=null){
                System.out.println("\n\n The Hand of "+ handWinnerExr.getNombre()+" is the hand winner. \n\n");
            }
        }
    }

    private static void print(Hand[] hands){
        System.out.println();
        for (Hand h:hands) {
            System.out.print(h.getNombre() + "\t" + h.getPoints()+"\t");
            for (Card c:h.getCards()) {
                System.out.print((c!=null)?c+" ":"{ , } ");
            }
            System.out.println();
        }
    }
    private static Hand findWinner(Hand[] hands){
        int count = 0;

        for (Hand h:hands) {
            if(h.getPoints()==21){
                return h;
            }
        }

        for (Hand h:hands) {
            if(h.getPoints()<21){
                count++;
            }
        }

        if(count==1){
            for (Hand h:hands) {
                if(h.getPoints()<21){
                    return h;
                }
            }
        }
        return null;
    }

    private static Hand findWinnerDefault(Hand[] hands){
        Hand wiinerDefault = null;
            for (Hand h:hands) {
                if(wiinerDefault==null && h.getPoints()<21){
                    wiinerDefault = h;
                }else if(h.getPoints()<21 && h.getPoints()>wiinerDefault.getPoints()){
                    wiinerDefault = h;
                }
            }
            return wiinerDefault;
    }
}
