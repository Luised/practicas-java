package Pocker;

public class Hand {
    private String nombre;
    private int puntos = 0;
    private int cardsact=0;
    private Card[] cards = new Card[5];

    public Hand(String nombre) {
        this.nombre = nombre;
    }

    public boolean addCard(Card a){
        if (cardsact<5) {
            cards[cardsact++] = a;
            sumPoints();
            return true;
        }else {
            return false;
        }
    }

    private void sumPoints(){
        int total = 0;
        for (int i=0;i<cardsact;i++){
            total += cards[i].getValue();
        }
        puntos=total;
    }

    public int getPoints(){
        return this.puntos;
    }

    public Card[] getCards(){
        return this.cards;
    }

    public String getNombre() {
        return nombre;
    }
}
