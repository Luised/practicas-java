package Pocker;

public class Card {
    private int cardNumber;
    private cardType cardType;

    public Card(int cardNumber, cardType cardType) {
        this.cardNumber = cardNumber;
        this.cardType = cardType;
    }

    public int getValue() {
        return this.cardNumber;
    }

    @Override
    public String toString() {
        if(cardNumber<10){
            return "{ " + cardNumber + "," + cardType + '}';
        }
        return "{" + cardNumber + "," + cardType + '}';
    }

}
/*
class Hand{
    str nombre="hand1";
    int cardsact=2;
    array cards={2d,5t,null,null,null};
    puntos = 7;
}

class Hand{
    str nombre="hand2";
    int cardsact=5;
    array cards={2d,5t,7p,7c,null};
    puntos = 21;
    addCard(card a){
        if cardsact<5
        cards[4] = a;
        cardsact++;
    }
}

hand1.puntos = 21
//*/